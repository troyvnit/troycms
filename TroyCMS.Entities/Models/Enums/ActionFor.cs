﻿namespace TroyCMS.Entities.Models
{
    public enum ActionFor
    {
        Post, Comment
    }
}

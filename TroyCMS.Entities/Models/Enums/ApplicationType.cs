﻿namespace TroyCMS.Entities.Models.Enums
{
    public enum ApplicationType
    {
        JavaScript, NativeConfidential
    }
}

﻿using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TroyCMS.DataAccess;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Infrastructure.Identity;
using TroyCMS.Entities.Models;
using TroyCMS.DataAccess.Models;
using TroyCMS.DataAccess.Repositories;
using System.Data.Entity;

namespace TroyCMS.DataAccess.Services
{
    public class TagService : Service<Tag>, ITagService
    {
        private readonly IRepository<Tag> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly TroyCMSIdentityContext _ictx;
        private readonly TroyCMSUserManager _userManager;

        public TagService(IRepository<Tag> repository, IUnitOfWork unitOfWork) : base(repository)
        {
            this._repository = repository;
            this._unitOfWork = unitOfWork;
            _ictx = new TroyCMSIdentityContext();
            _userManager = new TroyCMSUserManager(new UserStore<User>(_ictx));
        }

        public async Task<IEnumerable<TagDTO>> SearchTags(string searchTerm, int take, int skip)
        {
            IQueryable<Tag> query = _repository.Queryable()
                .Where(t => t.FullName.Contains(searchTerm) || t.ShortName.Contains(searchTerm) || t.NickName.Contains(searchTerm) || t.Slug.Contains(searchTerm))
                .OrderByDescending(t => t.FullName)
                .Skip(skip)
                .Take(take);

            return await query.Select(t => new TagDTO()
            {
                Id = t.Id,
                FullName = t.FullName,
                Name = t.ShortName,
                NickName = t.NickName,
                Avatar = t.Avatar,
                Slug = t.Slug,
                SearchCount = t.SearchCount,
                Level = t.Level
            }
            ).ToListAsync();

        }

        public async Task<IEnumerable<TagDTO>> GetTop(int top, TagType type)
        {
            IQueryable<Tag> query = _repository.Queryable().Where(t => t.SearchCount > 0 && t.TagType == type)
                .OrderByDescending(t => t.SearchCount)
                .Take(top);

            var result = await query.ToListAsync();
            return result.Select(Mapper.Map<TagDTO>);
        }

        public async Task<TagDTO> GetTag(string slug)
        {
            var tag = await _repository.Queryable().Include(t => t.Posts).FirstOrDefaultAsync(t => t.Slug == slug);
            if(tag == null)
            {
                return null;
            }
            return Mapper.Map<TagDTO>(tag);
        }
    }
}

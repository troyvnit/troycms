﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Models;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Services
{
    public interface ITagService : IService<Tag>
    {
        Task<IEnumerable<TagDTO>> SearchTags(string searchTerm, int take, int skip);
        Task<IEnumerable<TagDTO>> GetTop(int top, TagType type);
        Task<TagDTO> GetTag(string slug);
    }
}

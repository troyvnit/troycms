﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Models;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Services
{
    public interface IPlayerService : IService<Player>
    {
        IEnumerable<GetPlayerDTO> GetPlayers(int skip, int take, string userName, ref int count);
        Task<GetPlayerDTO> CreateOrUpdatePlayer(NewPlayerDTO newPlayer, string userName);
    }
}

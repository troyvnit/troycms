﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.DataAccess.Models;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Services
{
    public interface ITeamService : IService<Team>
    {
        IEnumerable<GetTeamDTO> GetTeams(int skip, int take, string userName, ref int count);
        Task<GetTeamDTO> CreateOrUpdateTeam(NewTeamDTO newTeam, string userName);
    }
}

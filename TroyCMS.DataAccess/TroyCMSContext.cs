﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using TroyCMS.DataAccess.Infrastructure;
using TroyCMS.Entities;
using TroyCMS.Entities.Models;
using TroyCMS.Entities.Models.Configurations;

namespace TroyCMS.DataAccess
{
    public class TroyCMSContext : DataContext
    {
        static TroyCMSContext()
        {
            Database.SetInitializer<TroyCMSContext>(null);
        }
        public TroyCMSContext() : base("Name=TroyCMSContext") { }

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Video> Videos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // This needs to go before the other rules!
            
            modelBuilder.Ignore<IdentityUserRole>();
            modelBuilder.Ignore<IdentityUserLogin>();
            modelBuilder.Ignore<IdentityUserClaim>();
            modelBuilder.Ignore<IdentityRole>();
            modelBuilder.Ignore<IdentityUser>();
        }
    }
}

﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using TroyCMS.Entities.Models;

namespace TroyCMS.DataAccess.Infrastructure.Identity
{
    public class TroyCMSUserManager : UserManager<User>
    {
        public TroyCMSUserManager(IUserStore<User> store)
            : base(store)
        {
    }

    public static TroyCMSUserManager Create(IdentityFactoryOptions<TroyCMSUserManager> options, IOwinContext context)
    {
        var troyCMSIdentityContext = context.Get<TroyCMSIdentityContext>();
        var troyCMSUserManager = new TroyCMSUserManager(new UserStore<User>(troyCMSIdentityContext));

        return troyCMSUserManager;
    }
}
}
